<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd" >
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
request.setAttribute("contextName", request.getContextPath());
%>
<html>
<head>
<script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="bg-sky-500">
	<div class="text-bold text-4xl text-center p-5">
		<p>FORM MAHASISWA</p>
	</div>
	<div class="md:flex md:justify-center mb-6">
		<form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
			action="${contextName}/mhs" method="post">
			<div>
				<input type="hidden" name="mode" value="tambah">
			</div>
			<div class="py-2">
				<label class="text-sm font-bold"> NIM </label> <input
					class="shadow appearance-none border rounded-lg w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
					type="text" name="nim" value="" placeholder="NIM">
			</div>
			<div class="py-2">
				<label class="text-sm font-bold"> Nama </label> <input
					class="shadow appearance-none border rounded-lg w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
					type="text" name="nama" value="" placeholder="Nama">
			</div>
			<div class="py-2">
				<label class="text-sm font-bold"> Tanggal Lahir </label> <input
					class="shadow appearance-none border rounded-lg w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
					type="date" name="tgl_lahir" value="">
			</div>
			<div class="py-2">
				<label class="text-sm font-bold"> Alamat </label> <input
					class="shadow appearance-none border rounded-lg w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
					type="text" name="alamat" value="" placeholder="Alamat">
			</div>
			<div class="py-2">
				<label class="text-sm font-bold"> Jurusan </label> <input
					class="shadow appearance-none border rounded-lg w-full py-2 px-3 text-black leading-tight focus:outline-none focus:shadow-outline"
					type="text" name="jurusan" value="" placeholder="Jurusan">
			</div>
			<div class="text-center py-4">
				<button
					class="bg-sky-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded-full"
					type="submit">Simpan</button>
			</div>
		</form>
	</div>
	<div class="overflow-auto py-5 md:flex md:justify-center mb-6">
		<table
			class="table-auto w-full text-sm text-left text-gray-500 dark:text-gray-400 max-w-xs">
			<thead
				class="text-xs uppercase bg-gray-50 dark:bg-gray-700 dark:text-sky-500">
				<tr>
					<th scope="col" class="py-3 px-6">Daftar Mahasiswa</th>
					<th scope="col" class="py-3 px-6">Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="mhs" items="${mhslist}">
					<tr class="border-b-4 bg-white dark:border-gray-700">
						<th scope="row"
							class="py-4 px-6 font-medium text-black whitespace-nowrap">
							${mhs.nama}</th>
						<td class="py-4 px-6"><a style="color: red;"
							href="${contextName}/mhs/edit?nim=${mhs.nim}">Edit</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>