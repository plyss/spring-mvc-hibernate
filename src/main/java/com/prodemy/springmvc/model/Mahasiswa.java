/**
 * 
 */
package com.prodemy.springmvc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wyant
 *
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "data_mahasiswa")
public class Mahasiswa {
	@Id
	@Column(name = "nim")
	private String nim;
	
	@Column(name = "nama")
	private String nama;
	
	@Column(name = "tgl_lahir")
	private String tgl_lahir;
	
	@Column(name = "alamat")
	private String alamat;
	
	@Column(name = "jurusan")
	private String jurusan;

}
